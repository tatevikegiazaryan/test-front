export const getters = {
  loggedIn: (state) => () => {
		return state.token !== null
    },
};