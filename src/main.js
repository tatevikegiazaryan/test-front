import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import VueRouter from 'vue-router'
import router from './routes'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { store } from './store/index.js';
import axios from 'axios'
axios.defaults.baseURL = 'http://project.test/api';
axios.defaults.headers.common['Authorization'] = "Bearer " + localStorage.getItem('access_token');
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(Vuex);
Vue.config.productionTip = false


router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn()) {
      next({
        path: '/login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresAuth === false)) {
      if (store.getters.loggedIn()) {
        next({
          path: '/reports',
        })
      } else {
        next()
      }
  } else {
    next()
  }
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
