import axios from 'axios'
const domain = 'http://project.test/api';


const apiClient = axios.create({
	baseURL: domain,
    headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + localStorage.getItem('access_token')
	
    }
});
export default apiClient;
