import VueRouter from 'vue-router'
import AllReport from './components/AllReport'
import Login from './components/Login'
import Registration from './components/Registration'
import CreateReport from './components/CreateReport'
import CreateReportType from './components/CreateReportType'
import EditReport from './components/EditReport'

export default new VueRouter ({
  routes: [
  {
    path: '/reports/:type?',
    name: 'reports',
    component: AllReport,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/edit/:id?',
    name: 'edit',
    component: EditReport,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/login',
    alias: '/',
    name: 'login',
    component: Login,
    meta: {
      requiresAuth: false,
    }
  },
  {
    path: '/registration',
    component: Registration,
    meta: {
      requiresAuth: false,
    }
  },
  {
    path: '/create-report',
    name: 'create-report',
    component: CreateReport,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: '/create/:type',
        component: CreateReportType,
        name:'create',
      }
    ]
  },
],
  mode: 'history'
})
