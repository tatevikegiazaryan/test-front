import axios from '../axios.js';


export const actions = {
  createReport(context, data) {
      console.log(data)
         return new Promise((resolve, reject) => {
        axios.post('/create-report', {
          title:data.title,
          content:data.content,
          type:data.type,

        })
          .then(response => {
            console.log(response)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    register(context, data) {
      return new Promise((resolve, reject) => {
        axios.post('/register', {
          name:data.name,
          email:data.email,
          password:data.password,
        })
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    logout(context) {
      if(context.getters.loggedIn()){
        return new Promise((resolve, reject) => {
          axios.post('/logout')
            .then(response => {
              localStorage.removeItem('access_token');
              context.commit('logout');
              resolve(response)
            })
            .catch(error => {
              localStorage.removeItem('access_token');
              context.commit('logout');
              console.log(error.message);
              reject(error)
            })
        })
      }
  },
  loginForm(context, credentials) {
      return new Promise((resolve, reject) => {
          axios.post('/login', {
            email: credentials.email,
            password: credentials.password
          })
            .then(response => {
              const token = response.data.access_token;
              localStorage.setItem('access_token', token);
              context.commit('loginForm', token);
              resolve(response)
            })
            .catch(error => {
              reject(error)
            })
      })
    }
  
}